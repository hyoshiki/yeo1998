'''
  Q(r) calculator in Secetary Problem
    based on Yeo's equation
             written by Hugh Yoshiki
                               13 Apr 2017
  mail: yoshiki.h.aa@m.titech.ac.jp

'''

import probability as prob
import interviewcounts as ivct
import dataprocessor as dpro
# If you want to read debug messages, replace the modules
#import debugprobability as prob
#import debuginterviewcounts as ivct

'''
FUNCTION PROTOTYPES
prob.main(a, n, r1, r2, r3) = P(a|r)
ivct.sigmaPr(k, r1, r2, r3, n) = Sigma(Pr{T=j|r})
dpro.csvread('filename.csv') = (r_1st, r_2nd, r_3rd)
dpro.outputQr(name, subjects, qr1, qr2, qr3)
'''

# initialization
n = 40
k = 3
c = 0  #TODO: c, cost per interview is not implemented
       # as c=0, in process below, ivct.sigmaPi is disregarded.
P = [0] * k
Q_r_1st = []
Q_r_2nd = []
Q_r_3rd = []
name_of_csv = '170227-data-short.csv'

'''
# cast csv into 3 tuples: r_1st, r_2nd, r_3rd.
# r_1st[p][q] = 1st_q-st_threshold of subject #p
# r_2nd[p][q] = 2nd_q-st_threshold of subject #p
# r_3rd[p][q] = 3rd_q-st_threshold of subject #p
'''

subjects, r_1st, r_2nd, r_3rd = dpro.csvread(name_of_csv)
subject_id = range(1, subjects+1)

w_a = (40, 39, 38) #weighting coef.

for p in range(subjects):
    # Q(r) for 1st threshold
    r = (r_1st[p][0], r_1st[p][1], r_1st[p][2])
    for q in range(k):
        P[q] = prob.main(q+1, n, r[0], r[1], r[2])
    Q_r_1st.append(P[0]*w_a[0]+P[1]*w_a[1]+P[2]*w_a[1] - ivct.sigmaPr(k, r[0], r[1], r[2], n) * c)
    # Q(r) for 2nd threshold
    r = (r_2nd[p][0], r_2nd[p][1], r_2nd[p][2])
    for q in range(k):
        P[q] = prob.main(q+1, n, r[0], r[1], r[2])
    Q_r_2nd.append(P[0]*w_a[0]+P[1]*w_a[1]+P[2]*w_a[1] - ivct.sigmaPr(k, r[0], r[1], r[2], n) * c) 
    # Q(r) for 3rd threshold
    r = (r_3rd[p][0], r_3rd[p][1], r_3rd[p][2])
    for q in range(k):
        P[q] = prob.main(q+1, n, r[0], r[1], r[2])
    Q_r_3rd.append(P[0]*w_a[0]+P[1]*w_a[1]+P[2]*w_a[1] - ivct.sigmaPr(k, r[0], r[1], r[2], n) * c) 
    
dpro.outputQr('output.csv', subject_id, Q_r_1st, Q_r_2nd, Q_r_3rd)

print('main.py finished processing & Q(r) calculation with '+ name_of_csv + '!!')
