'''
 [DEBUGGING VERSION]
  Probability of last interview number in Secetary Problem
    based on Yeo's equation (2) - (4)
             written by Hugh Yoshiki
                               09 Apr 2017
'''

# Pi
def normalPi(d, j, r1, r2, r3, n):
    r = (r1, r2, r3, n)
    numer = 1
    denom = 1
    # multiplying
    for i in range(d):
        i += 1
        numer *= r[i-1] - i
        denom *= j - i
    result = numer / denom * d / j
    print("debug: Pi for interview counts with j=" +str(j)+" was calculated as "+str(result))
    return result

def lastPi(k, r1, r2, r3, n):
    r = (r1, r2, r3, n)
    print("debug: r tuple in lastPi is "+str(r))
    numer = 1
    denom = 1
    # multiplying
    print("debug: in lastPi, # of iterations will be: "+str(k))
    for i in range(k):
        print("debug: in lastPi, at first i="+str(i))
        i += 1
        print("debug: in lastPi, 1 added to i then i="+str(i))
        numer *= r[i-1] - i
        denom *= n - i
    result = numer / denom
    print("debug: Pi for interview counts with j=" +str(n)+" was calculated as "+str(result))
    return result

def sigmaPr(k, r1, r2, r3, n):
    result = 0
    for j in range(r1, r2):
        d = 1
        result += normalPi(d, j, r1, r2, r3, n)
    for j in range(r2, r3):
        d = 2
        result += normalPi(d, j, r1, r2, r3, n)
    for j in range(r3, n):
        d = 3
        result += normalPi(d, j, r1, r2, r3, n)
    result += lastPi(k, r1, r2, r3, n)
    print("debug: sigmaPr for interview counts was calculated as "+str(result))
    return result
