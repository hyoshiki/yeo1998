'''
  [DEBUGGING VERSION]
  Probability of success in Secetary Problem
    based on Yeo's equation (2) - (4)
             written by Hugh Yoshiki
                               09 Apr 2017
'''
# Binominal coefficients
def ncp(n,p):
    result = 1
    if n == 0 or p == 0:
        return result

    for i in range(p):
        result *= n - i
        result /= i + 1
    return result

# Sigma at the center of the equation
def thirdSigma(d,a,j,n):
    num = 0
    d_hat_a = d
    if d > a:
    	d_hat_a = a
    # summing up
    for s in range(d_hat_a):
    	s += 1
    	this_num = ncp(j-1, s-1) * ncp(n-j, a-s)
    	num += this_num
    	print("debug: This is iteration #" + str(s) + " for the thirdSigma with j=" + str(j))
    	print("debug: "+str(j-1)+"C"+str(s-1)+" * "+str(n-j)+"C"+str(a-s)+" = "+str(this_num))
    return num

# Pi at the middle
def middlePi(d, j, r1, r2, r3, n):
    numer = 1
    denom = 1
    r = (r1, r2, r3)
	# multiplying
    for i in range(d):
        i += 1
        numer *= r[i-1] - i
        denom *= j - i
        print ("debug: This is iteration #" + str(i) + " for the middlePi with j=" + str(j))
        print("debug: numer/denom= " +str(numer)+"/"+str(denom)+" ... iteration #"+ str(i))
    result = numer / denom
    print ("debug: numer/denom = "+ str(result))
    return result

# Main calculation process
def main(a, n, r1, r2, r3):
    events = 0
    r = (r1, r2, r3, n) 
    for k in (1, 2, 3):  # this is the tuple for k
        d = k
        j = r[k-1]
        if k <3:
            fin = r[k]
        else:
            fin = n
        while j < fin:
        	mP = middlePi(d, j, r1, r2, r3, n)
        	tS = thirdSigma(d, a, j, n)
        	events += mP*tS
        	print("debug: iteration finished: " + str(events) + " with d=" + str(d) + ", j=" + str(j) + " , a=" + str(a) + ", n=" + str(n))
        	print("")
        	j += 1
        print("debug: main loop for k is done.")
    print("debug: loop for events counting is over")
    print("debug: Events counted: " + str(events))
    whole_events = n * ncp(n-1, a-1)
    print("debug: whole_events=" +str(whole_events))
    if whole_events < events:
    	print("error: invalid value for events")
    	print("events: " + "{0:.5f}".format(events) + ", whole: " + str(whole_events))
    	result = 0
    else:
    	result = events / whole_events

    print("")
    print("From probability.py:")
    print("Final result for P("+str(a)+") is derived!")
    print("//////////////////////////////////")
    print("P("+str(a)+")= " + "{0:.4f}".format(result))
    print("//////////////////////////////////")
    print("")
    return result
