'''
 Data Processor module for Q(r) calculator
             written by Hugh Yoshiki
                               13 Apr 2017
'''
import numpy as np

def csvread(name):
    data=np.loadtxt(name, comments='no', delimiter=',', dtype='float')
    rows = data.shape[0]
    r_1st, r_2nd, r_3rd = [], [], []
    for i in range(rows):
        r_1st.append( (int(data[i, 1]), int(data[i, 2]), int(data[i, 3])) )
        r_2nd.append( (int(data[i, 5]), int(data[i, 6]), int(data[i, 7])) )
        r_3rd.append( (int(data[i, 9]), int(data[i, 10]), int(data[i, 11])) )
#    print(r_1st)
#    print(r_2nd)
#    print(r_3rd)
    return (rows,  r_1st, r_2nd, r_3rd)

def outputQr(name, subjects, qr1, qr2, qr3):
    num = np.array(subjects)
    q1 = np.array(qr1)
    q2 = np.array(qr2)
    q3 = np.array(qr3)
    output = np.c_[num,q1,q2,q3]
    label = 'subject #, Q(r) for 1st threshold, Q(r) for 2nd threshold, Q(r) for 3rd threshold'
    np.savetxt(name, output, delimiter=',', fmt='%.6f', header=label)
    print(name+' is created as an output!')
