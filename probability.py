'''
 Probability of success in Secetary Problem
    based on Yeo's equation (2) - (4)
             written by Hugh Yoshiki
                               09 Apr 2017
'''
# Binominal coefficients
def ncp(n,p):
    result = 1
    if n == 0 or p == 0:
        return result

    for i in range(p):
        result *= n - i
        result /= i + 1
    return result

# Sigma at the center of the equation
def thirdSigma(d,a,j,n):
    num = 0
    d_hat_a = d
    if d > a:
    	d_hat_a = a
    # summing up
    for s in range(d_hat_a):
    	s += 1
    	this_num = ncp(j-1, s-1) * ncp(n-j, a-s)
    	num += this_num
    return num

# Pi at the middle
def middlePi(d, j, r1, r2, r3, n):
    numer = 1
    denom = 1
    r = (r1, r2, r3)
	# multiplying
    for i in range(d):
        i += 1
        numer *= r[i-1] - i
        denom *= j - i
    result = numer / denom
    return result

# Main calculation process
def main(a, n, r1, r2, r3):
    events = 0
    r = (r1, r2, r3, n)
    for k in (1, 2, 3):  # this is the tuple for k
        d = k
        j = r[k-1]
        if k <3:
            fin = r[k]
        else:
            fin = n
        while j < fin:
        	mP = middlePi(d, j, r1, r2, r3, n)
        	tS = thirdSigma(d, a, j, n)
        	events += mP*tS
        	j += 1
    whole_events = n * ncp(n-1, a-1)
    if whole_events < events:
    	result = 0
    else:
    	result = events / whole_events

#    print("From probability.py:P("+str(a)+")= " + "{0:.4f}".format(result))
    return result
