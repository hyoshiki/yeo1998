'''
  Probability of last interview number in Secetary Problem
    based on Yeo's equation (2) - (4)
             written by Hugh Yoshiki
                               09 Apr 2017
'''

# Pi
def normalPi(d, j, r1, r2, r3, n):
    r = (r1, r2, r3, n)
    numer = 1
    denom = 1
    # multiplying
    for i in range(d):
        i += 1
        numer *= r[i-1] - i
        denom *= j - i
    result = numer / denom * d / j
    return result

def lastPi(k, r1, r2, r3, n):
    r = (r1, r2, r3, n)
    numer = 1
    denom = 1
    # multiplying
    for i in range(k):
        i += 1
        numer *= r[i-1] - i
        denom *= n - i
    result = numer / denom
    return result

def sigmaPr(k, r1, r2, r3, n):
    result = 0
    for j in range(r1, r2):
        d = 1
        result += normalPi(d, j, r1, r2, r3, n)
    for j in range(r2, r3):
        d = 2
        result += normalPi(d, j, r1, r2, r3, n)
    for j in range(r3, n):
        d = 3
        result += normalPi(d, j, r1, r2, r3, n)
    result += lastPi(k, r1, r2, r3, n)
    return result
